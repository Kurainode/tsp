import argparse
import random
import numpy as np
import operator
import pandas as pd
import matplotlib.pyplot as plt

distance_matrix = np.zeros((0, 0));

class City:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.index = 0

    def distance(self, city):
        xDis = abs(self.x - city.x)
        yDis = abs(self.y - city.y)
        distance = np.sqrt((xDis ** 2) + (yDis ** 2))
        return distance

    def __repr__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"


class Fitness:
    def __init__(self, route):
        self.route = route
        self.distance = 0
        self.fitness = 0.0

    def routeDistance(self):
        global distance_matrix
        if self.distance == 0:
            pathDistance = 0
            for i in range(0, len(self.route)):
                fromCity = self.route[i]
                toCity = self.route[i + 1] if (i + 1 < len(self.route)) else self.route[0]
                pathDistance += distance_matrix[fromCity.index, toCity.index]
            self.distance = pathDistance
        return self.distance

    def routeFitness(self):
        if self.fitness == 0:
            self.fitness = float(self.routeDistance())
        return self.fitness

def createRoute(cityList):
    route = random.sample(cityList, len(cityList))
    return route

def initialPopulation(popSize, cityList):
    population = []

    for i in range(0, popSize):
        population.append(createRoute(cityList))
    return population

def rankRoutes(population):
    fitnessResults = {}
    for i in range(0,len(population)):
        fitnessResults[i] = Fitness(population[i]).routeFitness()
    return sorted(fitnessResults.items(), key = operator.itemgetter(1))


def selection(popRanked, eliteSize):
    selectionResults = []
    df = pd.DataFrame(np.array(popRanked), columns=["Index", "Fitness"])
    df['cum_sum'] = df.Fitness.cumsum()
    df['cum_perc'] = 100 * df.cum_sum / df.Fitness.sum()

    for i in range(0, eliteSize):
        selectionResults.append(popRanked[i][0])
    for i in range(0, len(popRanked) - eliteSize):
        pick = 100 * random.random()
        for i in range(0, len(popRanked)):
            if pick <= df.iat[i, 3]:
                selectionResults.append(popRanked[i][0])
                break
    return selectionResults

def matingPool(population, selectionResults):
    matingpool = []
    for i in range(0, len(selectionResults)):
        index = selectionResults[i]
        matingpool.append(population[index])
    return matingpool


def breed(parent1, parent2):
    child = []
    childP1 = []
    childP2 = []

    geneA = int(random.random() * len(parent1))
    geneB = int(random.random() * len(parent1))

    if (geneA <= geneB):
        for i in range(geneA, geneB):
            childP1.append(parent1[i])
    else:
        for i in range(geneB, geneA + len(parent1)):
            childP1.append(parent1[i % len(parent1)])

    childP2 = [item for item in parent2 if item not in childP1]

    child = childP1 + childP2
    return child


def breedPopulation(matingpool, eliteSize):
    children = []
    length = len(matingpool) - eliteSize
    elite = []

    for i in range(0, eliteSize):
        children.append(matingpool[i])
        elite.append(matingpool[i])

    for i in range(0, length):
        child = breed(elite[int(i / eliteSize) + 1], elite[int((i + 1) % eliteSize)])
        children.append(child)
    return children


def mutate(individual, mutationRate):
    for swapped in range(len(individual)):
        if (random.random() < mutationRate):
            swapWith = int(random.random() * len(individual))

            for i in range(int((swapWith - swapped) / 2)):
                city1 = individual[swapped + i]
                city2 = individual[(swapWith - i) % len(individual)]
                individual[swapped + i] = city2
                individual[(swapWith - i) % len(individual)] = city1
    return individual


def mutatePopulation(population, mutationRate, eliteSize):
    mutatedPop = []

    for ind in range(0, len(population)):
        if (ind > eliteSize):
            mutatedInd = mutate(population[ind], mutationRate)
            mutatedPop.append(mutatedInd)
        else:
            mutatedPop.append(population[ind])
    return mutatedPop

def nextGeneration(currentGen, eliteSize, mutationRate):
    popRanked = rankRoutes(currentGen)
    selectionResults = selection(popRanked, eliteSize)
    matingpool = matingPool(currentGen, selectionResults)
    children = breedPopulation(matingpool, eliteSize)
    nextGeneration = mutatePopulation(children, mutationRate, eliteSize)
    return nextGeneration


def geneticAlgorithm(population, popSize, eliteSize, mutationRate, generations):
    global distance_matrix
    base_mutation = mutationRate
    distance_matrix = np.zeros((len(population), len(population)))
    for x in range(len(population)):
        population[x].index = x
        for y in range(len(population)):
            distance = population[x].distance(population[y])
            distance_matrix[x, y] = distance
            distance_matrix[y, x] = distance
    pop = initialPopulation(popSize, population)
    print("Initial distance: " + str(rankRoutes(pop)[0][1]))

    plt.axis([0, generations, 0, rankRoutes(pop)[0][1]])
    stuck_counter = 0
    best_route_len = rankRoutes(pop)[0][1]

    for i in range(0, generations):
        pop = nextGeneration(pop, eliteSize, mutationRate)
        ranked_routes = rankRoutes(pop)
        if best_route_len > ranked_routes[0][1]:
            stuck_counter = 0
            mutationRate = base_mutation
        else:
            stuck_counter += 1

        if stuck_counter > 100:
            mutationRate = base_mutation * 2;

        best_route_len = ranked_routes[0][1]

        print("Generation:", i, "Best distance:", ranked_routes[0][1])
        if (i % 10 == 0):
            plt.plot(i, ranked_routes[0][1], 'xb--', linewidth=1, markersize=1)
            plt.pause(0.01)

    print("Final distance: " + str(rankRoutes(pop)[0][1]))
    bestRouteIndex = rankRoutes(pop)[0][0]
    bestRoute = pop[bestRouteIndex]
    return bestRoute

def main():
    parser = argparse.ArgumentParser(description='Tries to find the shortest path between cities')
    parser.add_argument('location', metavar='file', type=str, help='TSP data file location')
    args = parser.parse_args()
    dataset = []
    file = open(args.location, 'r')
    for line in file:
        if all([c.isdigit() or c == '\n' or c == '\r' or c == ' ' for c in line]):
            string_list = line.split(' ')
            int_list = [int(i) for i in string_list]
            if len(int_list) == 3:
                dataset.append(City(x=int_list[1], y=int_list[2]))
    if len(dataset) > 0:
        geneticAlgorithm(population=dataset, popSize=200, eliteSize=20, mutationRate=0.006, generations=3000)
        plt.show()

if __name__ == "__main__":
    main()